# Register CS-490

This project allows you to register for CS 490. Before registering, note:
* You must already have a GitLab account. 
* You may use a pseudonym to conceal your name

In order to register for this class:
1.  Go to the issue tracker by clicking on the "Issues" link in the left menu
2.  Open a new issue by pressing the "New Issue" button
3.  In the Title box enter: Register <your gitlab name>
4.  In the Description area enter:
    1.  Your GitLab handle
    2.  Your full name (first and last)
7.  Before hitting "Submit Issue", **check the box** before "This issue is confidential and should only be visible to team members with at least Reporter access."

